const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const app = express();
const port = 3000;

// const ParkingLocationsSchema = new mongoose.Schema({
//     Title: String,
//     Description: String,
//     city: String,
//     street_address: String,
//     postcode: String,
//     lat: {
//       type: Number, // Define the type as Number
//       min: -90,
//       max: 90
//     },
//     lon: {
//       type: Number, // Define the type as Number
//       min: -180,
//       max: 180
//     },
//     spaces_available: Number,
//     total_spaces: Number
//   });
  
const ParkingLocationsSchema = new mongoose.Schema({
  Title: String,
  Description: String,
  city: String,
  street_address: String,
  postcode: String,
  location: {
      type: {
          type: String,
          enum: ['Point'],
          required: true
      },
      coordinates: {
          type: [Number],
          required: true
      }
  },
  spaces_available: Number,
  total_spaces: Number
});

// Add a 2dsphere index on the location field for geospatial queries
ParkingLocationsSchema.index({ location: '2dsphere' });


const ParkingLocation = mongoose.model('ParkingLocations', ParkingLocationsSchema);

module.exports = ParkingLocation;
