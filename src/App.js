const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const app = express();
const ParkingLocation = require('./model/ParkingLocation');
const bodyParser = require('body-parser');
const jwt = require('jwt-simple');
const { postcodeValidator, postcodeValidatorExistsForCountry } = require('postcode-validator');
const axios = require('axios');

app.use(bodyParser.json());
app.use(cors());

const port = process.env.PORT;
const JWT_ALGORITHM = 'HS512';

app.get('/location/parking-locations-all', isAdmin, async (request, response) => {
    try {
        const parkingLocations = await ParkingLocation.find({});
        // console.log(`🤖: Successfully retrieved all ${parkingLocations.length} parking locations:`);
        response.send(parkingLocations);
    } catch (x) {
        console.error(x);
        response.status(500).send('⛔️ Server Error: An error has occured, please try again later');
    }
});

app.get('/location/parking-location/:id', async (request, response) => {
    try {
        const id = request.params.id;
        if (!id){
            return response.status(400).json({ error: '⛔️: Please provide a valid parking location id.' });
        }
        
        const parkingLocation = await ParkingLocation.findById(id);

        if (!parkingLocation) {
            return response.status(400).json({ error: '⛔️: Parking location not found.' });
        }

        console.log(`🤖: Successfully retrieved parking location with id ${id}:`);

        return response.send(parkingLocation);

    } catch (error) {
        return response.status(500).json({ error: '⛔️: Failed to retrieve parking location, please try again later.' });
    }
    
});

app.post('/location/parking-location/decrement/:id',async (request, response) => {
    try {

        const bearerHeader = request.headers['authorization'];
        if (!bearerHeader) {
            return response.status(400).json({ error: '⛔️: Please provide a valid MASTER token.' });
        }

        console.log('bearer header:', bearerHeader);

        const adminToken = bearerHeader.split(' ')[1];

        if (!adminToken) {
            return response.status(400).json({ error: '⛔️: Please provide a valid MASTER token.' });
        }

        if (adminToken !== process.env.ADMIN_TOKEN) {
            console.error(adminToken + ' is not equal to ' + process.env.ADMIN_TOKEN)
            return response.status(401).json({ error: '⛔️: Master token provided is invalid' });
        }

        const id = request.params.id;
        if (!id){
            return response.status(400).json({ error: '⛔️: Please provide a valid parking location id.' });
        }
        
        const parkingLocation = await ParkingLocation.findById(id);

        if (!parkingLocation) {
            return response.status(400).json({ error: '⛔️: Parking location not found.' });
        }

        if (parkingLocation.spaces_available >= 1) {
            parkingLocation.spaces_available -= 1;
            await parkingLocation.save();
        }else {
            return response.status(500).json({ error: '⛔️: Parking location has no available spaces.' });
        }

        console.log(`🤖: Successfully retrieved parking location with id ${id}:`);

        return response.send(parkingLocation);

    } catch (error) {
        return response.status(500).json({ error: '⛔️: Failed to incrementing parking location spaces, please try again later.' });
    }
    
});

app.post('/location/parking-location/increment/:id', async (request, response) => {
    try {

        const bearerHeader = request.headers['authorization'];
        if (!bearerHeader) {
            return response.status(400).json({ error: '⛔️: Please provide a valid MASTER token.' });
        }

        const adminToken = bearerHeader.split(' ')[1];

        if (!adminToken) {
            return response.status(400).json({ error: '⛔️: Please provide a valid MASTER token.' });
        }

        if (adminToken !== process.env.ADMIN_TOKEN) {
            return response.status(401).json({ error: '⛔️: Master token provided is invalid' });
        }

        const id = request.params.id;
        if (!id){
            return response.status(400).json({ error: '⛔️: Please provide a valid parking location id.' });
        }
        
        const parkingLocation = await ParkingLocation.findById(id);

        if (!parkingLocation) {
            return response.status(400).json({ error: '⛔️: Parking location not found.' });
        }

        if (parkingLocation.spaces_available < parkingLocation.total_spaces ) {
            parkingLocation.spaces_available += 1;
            await parkingLocation.save();
        }else {
            return response.status(500).json({ error: `⛔️: Parking location already has ${ parkingLocation.total_spaces} spaces.` });
        }

        console.log(`🤖: Successfully retrieved parking location with id ${id}:`);

        return response.send(parkingLocation);

    } catch (error) {
        return response.status(500).json({ error: '⛔️: Failed to incrementing parking location spaces, please try again later.' });
    }
    
});

app.get('/location/parking-locations', async (request, response) => {
    try {
        const { postcode, radius } = request.query;
        console.log('postcode:', postcode);
        console.log('radius:', radius);

        // Validate UK postcode
        if (!postcodeValidator(postcode, 'GB')) {
            console.log('Invalid postcode format');
            return response.status(400).send('⛔️ Invalid postcode format');
        }

        // Use OpenStreetMap Nominatim API to get latitude and longitude
        const url = `https://nominatim.openstreetmap.org/search?q=${postcode}&format=json&limit=1`;
        const result = await axios.get(url);
        console.log('Nominatim API result:', result.data);
        const { lat, lon } = result.data[0];
        console.log('lat:', lat);
        console.log('lon:', lon);

        // Query the database for parking locations
        const parkingLocations = await ParkingLocation.find({
            location: {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: [parseFloat(lon), parseFloat(lat)]
                    },
                    $maxDistance: radius * 1609.34 // convert miles to meters
                }
            }
        });
        console.log('parkingLocations:', parkingLocations);

        console.log(`🤖: Successfully retrieved ${parkingLocations.length} parking locations near ${postcode}:`);
        response.send(parkingLocations);
    } catch (x) {
        console.error(x);
        response.status(500).send('⛔️ Server Error: An error has occurred, please try again later');
    }
});




function isAdmin(request, response, next) {
    const authHeader = request.headers.authorization;
    const token = authHeader && authHeader.split(' ')[1];
    if (!token) {
        response.status(401).json({ error: '⛔️: Access denied, please provide a valid token.' });
        return;
    }
    try {
        const user = jwt.decode(token, process.env.JWT_SECRET, false, JWT_ALGORITHM);
        if (user && user.isAdmin) {
            console.log('🤖: User is admin, proceed.')
            request.user = user;
            next();
        } else {
            response.status(403).json({ error: '⛔️: Only admin can make modifications.' });
        }
    } catch (err) {
        console.error(err);
        response.status(401).json({ error: '⛔️: Invalid token.' });
    }
}


app.post('/location/parking-locations', isAdmin, async (request, response) => {
    try {
        const {
            Title,
            Description,
            city,
            street_address,
            postcode,
            lat,
            lon,
            spaces_available,
            total_spaces
        } = request.body;
        const existingLocation = await ParkingLocation.findOne({ street_address });
        if (existingLocation) {
            return response.status(400).json({ error: '⛔️: A parking location with this street address already exists. Please try another alternative location. ' });
        }

        // Use the lat and lon coordinates to create a GeoJSON point
        const point = {
            type: "Point",
            coordinates: [parseFloat(lon), parseFloat(lat)]
        };

        const parkingLocation = new ParkingLocation({
            Title,
            Description,
            city,
            street_address,
            postcode,
            location: point,
            spaces_available,
            total_spaces,
        });
        console.log('🤖 Request Body:', request.body);
        await parkingLocation.save();
        response.status(201).json({ message: '🤖: Parking was successfully added to database.' });
    } catch (x) {
        console.error(x);
        response.status(500).json({ error: '⛔️: Failed to add parking to database, please try again later.' });
    }
});


// app.post('/location/parking-locations', isAdmin, async (request, response) => {
//     try {
//       const { Title, Description,city, street_address, postcode, lat, lon, spaces_available, total_spaces } = request.body;
      
//       const existingLocation = await ParkingLocation.findOne({ street_address });
//       if (existingLocation) {
//         return response.status(400).json({ error: '⛔️: A parking location with this street address already exists. Please try another alternative location. ' });
//       }
  
//       const parkingLocation = new ParkingLocation({
//         Title,
//         Description,
//         city,
//         street_address,
//         postcode,
//         lat,
//         lon,
//         spaces_available,
//         total_spaces,
//       });
//       console.log('🤖 Request Body:', request.body);
//       await parkingLocation.save();
//       response.status(201).json({ message: '🤖: Parking was successfully added to database.' });
//     } catch (x) {
//       console.error(x);
//       response.status(500).json({ error: '⛔️: Failed to add parking to database, please try again later.' });
//     }
//   });


app.delete('/location/parking-locations/:id', isAdmin ,async (request, response) => {
    try {
        const parkingLocation = await ParkingLocation.findByIdAndDelete(request.params.id);
        if (!parkingLocation) {
            response.status(404).json({ error: '⛔️: Parking spot not found.' });
            return;
        }
        response.json({ message: '🤖: Parking spot deleted.' });
    } catch (x) {
        console.error(x);
        response.status(500).json({ error: '⛔️: Failed to delete parking spot, please try again later.' });
    }
});


(async ()=>{
    // Connect to the MongoDB database
    const databaseUrl = process.env.DB_URL;
    if (!databaseUrl) {
        console.error('⚡️[server]: Database url not found. Please set the DB_URL environment variable.');
        return;
    }
    try {
        console.log("Connecting to database: " + databaseUrl);
        await mongoose.connect(databaseUrl);
    }
    catch (e) {
        console.log('⛔️ Database Error: An error has occured, please try again later');
        console.log(e);
        // Do not continue if we cannot connect to the database
        return;
    }

    console.log('🤖: Successfully connected to:', databaseUrl);

    app.listen(port, () => {
        console.log(`🤖: Now tuning into http://localhost:${port}`);
    });

})();
