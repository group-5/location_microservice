
const {mongoose}  = require('mongoose');

mongoose.connect('mongodb://localhost:27017/ParkingLocations', { useNewUrlParser: true });

const parkingLocationSchema = new mongoose.Schema({
    ID: Number,
    Title: String,
    Description: String,
    lat: Number,
    lon: Number
});

const ParkingLocation = mongoose.model('ParkingLocations', parkingLocationSchema);

const db = mongoose.connection;

db.once('open', async () => {
  console.log('Connection to database successful!');

  try {
    const parkingLocations = await ParkingLocation.find({});
    console.log(`Retrieved ${parkingLocations.length} parking locations:`);
    parkingLocations.forEach(parkingLocation => {
      console.log(`ID: ${parkingLocation.ID}`);
      console.log(`Title: ${JSON.stringify(parkingLocation.Title)}`);
      console.log(`Description: ${JSON.stringify(parkingLocation.Description)}`);            
      console.log(`lat: ${parkingLocation.lat}`);
      console.log(`lon: ${parkingLocation.lon}`);
      console.log(`--------------------------------------------`);
    });
  } catch (err) {
    console.error(err);
  } finally {
    mongoose.connection.close();
    console.log('Connection to database closed.');
  }
});

db.on('error', (err) => {
  console.error('Connection error:', err);
});
