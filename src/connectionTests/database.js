const { MongoClient } = require('mongodb');

async function main(){
    const uri = "mongodb://localhost:27017";
    const client = new MongoClient(uri);

    try {
        // Connect to the MongoDB cluster
        await client.connect();

        // Access the "ParkingLocations" database
        const db = client.db("ParkingLocations");

        // Find all documents in the "ParkingLocations" collection and print their properties
        const documents = await db.collection("ParkingLocations").find().toArray();
        documents.forEach((doc) => {
            console.log(`ID: ${doc.ID}`);
            console.log(`Title: ${JSON.stringify(doc.Title)}`);
            console.log(`Description: ${JSON.stringify(doc.Description)}`);            
            console.log(`lat: ${doc.lat}`);
            console.log(`lat: ${doc.lon}`);
            console.log(`Address: ${JSON.stringify(doc.street_address)}`);
            console.log(`City: ${JSON.stringify(doc.spaces_available)}`);

            console.log(`Spaces Available: ${JSON.stringify(doc.spaces_available)}`);
            console.log(`--------------------------------------------`);
        });

    } catch (e) {
        console.error(e);
    } finally {
        await client.close();
    }
}

main().catch(console.error);
